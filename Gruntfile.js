module.exports = function(grunt) {
	grunt.initConfig({
		karma: {
			test: {
				configFile: "karma.conf.js",
				singleRun: true,
				reporters: ["dots", "coverage"]
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-karma");

	grunt.registerTask("default", ["karma"]);
};
