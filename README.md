# Game of Life

This application is based on Conway's Game of Life and its implementation was inspired by a Coderetreat session with
the following conditions:

- implement Conway's Game of Life
- 45 minutes time limit
- use TDD
- use no primitive types

This particular implementation uses a few primitive types for performance and readability reasons, but sticks to using
custom objects where appropriate.

## Installation

    npm install

## Tests

    npm test

## Usage

Create an instance of the gameboard:

    var board = new Board();

Add live fields to the board:

    board.addField(new Field(new Coordinates(1, 1)));

Progress the lifecycle:

    board.nextTurn();

Check fields:

    var field = board.getField(new Coordinates(1, 1));
    if (undefined === field) {
        // field is dead
    }
    if (field instanceof Field) {
        // field is alive
    }
