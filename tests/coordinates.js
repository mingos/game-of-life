describe("coordinates", function() {
	it("check equality", function() {
		// given
		var coords1 = new Coordinates(5, 8);
		var coords2 = new Coordinates(5, 8);
		// when
		// then
		expect(coords1.equals(coords2)).toBeTruthy();
		expect(coords2.equals(coords1)).toBeTruthy();
	});

	it("check inequality", function() {
		// given
		var coords1 = new Coordinates(5, 8);
		var coords2 = new Coordinates(4, 6);
		// when
		// then
		expect(coords1.equals(coords2)).toBeFalsy();
		expect(coords2.equals(coords1)).toBeFalsy();
	});

	it("return neighbour coordinates", function() {
		// given
		var coords = new Coordinates(1, 1);
		// when
		var neighbours = coords.getNeighbours();
		// then
		expect(neighbours.length).toBe(8);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(0, 0)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(0, 1)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(0, 2)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(1, 0)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(1, 2)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(2, 0)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(2, 1)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(2, 2)); }).length).toBe(1);
	});
});
