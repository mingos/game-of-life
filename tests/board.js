describe("Board", function() {
	var board;
	beforeEach(function() {
		board = new Board();
	});

	it("kills lonely cells", function() {
		// given
		board.addField(new Field(new Coordinates(0, 0)));
		// when
		board.nextTurn();
		// then
		expect(board.getField(new Coordinates(0, 0))).toBeUndefined();
	});

	it("kills cells with one live neighbour", function() {
		// given
		board.addField(new Field(new Coordinates(0, 0)));
		board.addField(new Field(new Coordinates(1, 0)));
		board.addField(new Field(new Coordinates(2, 0)));
		// when
		board.nextTurn();
		// then
		expect(board.getField(new Coordinates(0, 0))).toBeUndefined();
		expect(board.getField(new Coordinates(1, 0))).toBeDefined();
		expect(board.getField(new Coordinates(2, 0))).toBeUndefined();
	});

	it("kills cells with too many live neighbours", function() {
		// given
		board.addField(new Field(new Coordinates(0, 0)));
		board.addField(new Field(new Coordinates(1, 0)));
		board.addField(new Field(new Coordinates(2, 0)));
		board.addField(new Field(new Coordinates(0, 1)));
		board.addField(new Field(new Coordinates(1, 1)));
		// when
		board.nextTurn();
		// then
		expect(board.getField(new Coordinates(0, 0))).toBeDefined();
		expect(board.getField(new Coordinates(1, 0))).toBeUndefined(); // killed: 4 neighbours
		expect(board.getField(new Coordinates(2, 0))).toBeDefined();
		expect(board.getField(new Coordinates(0, 1))).toBeDefined();
		expect(board.getField(new Coordinates(1, 1))).toBeUndefined(); // killed: 4 neighbours
	});

	it("creates new fields with 3 live neighbours", function() {
		// given
		board.addField(new Field(new Coordinates(0, 0)));
		board.addField(new Field(new Coordinates(1, 0)));
		board.addField(new Field(new Coordinates(2, 0)));
		// when
		board.nextTurn();
		// then
		expect(board.getField(new Coordinates(1, 1))).toBeDefined();
		expect(board.getField(new Coordinates(1, -1))).toBeDefined();
	});

	it("gets a field's live neighbours", function() {
		// given
		board.addField(new Field(new Coordinates(0, 0)));
		board.addField(new Field(new Coordinates(1, 0)));
		board.addField(new Field(new Coordinates(2, 0)));
		// when
		var neighbours = board.getLiveNeighbours(new Coordinates(1, 0));
		// then
		expect(neighbours.length).toBe(2);
		expect(neighbours[0].coordinates.equals(new Coordinates(0, 0))).toBeTruthy();
		expect(neighbours[1].coordinates.equals(new Coordinates(2, 0))).toBeTruthy();
	});

	it("gets a field's dead neighbours", function() {
		// given
		board.addField(new Field(new Coordinates(1, 0)));
		board.addField(new Field(new Coordinates(1, 1)));
		board.addField(new Field(new Coordinates(1, 2)));
		// when
		var neighbours = board.getDeadNeighbours(new Coordinates(1, 1));
		// then
		expect(neighbours.length).toBe(6);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(0, 0)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(0, 1)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(0, 2)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(2, 0)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(2, 1)); }).length).toBe(1);
		expect(neighbours.filter(function(coords) { return coords.equals(new Coordinates(2, 2)); }).length).toBe(1);
	});
});
