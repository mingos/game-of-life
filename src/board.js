/**
 * The gameboard.
 * Holds the living fields and deals with lifecycle turns, kiling or spawning new fields.
 * @constructor
 */
var Board = function() {
	this.fields = [];
};

/**
 * Add a new live field
 * @param {Field} field
 */
Board.prototype.addField = function(field) {
	this.fields.push(field);
};

/**
 * Fetch a live field from a given set of coordinates
 * @param   {Coordinates} coordinates
 * @returns {Field|undefined}
 */
Board.prototype.getField = function(coordinates) {
	var result = this.fields.filter(function(field) {
		return field.coordinates.equals(coordinates);
	});
	return result.length ? result[0] : undefined;
};

/**
 * Progress the game by one turn
 */
Board.prototype.nextTurn = function() {
	var board = this;
	var deadNeighbours = {};

	// process live cells and extract dead neighbours
	var fields = this.fields.filter(function(field) {
		board.getDeadNeighbours(field.coordinates).forEach(function(coordinates) {
			deadNeighbours[coordinates.valueOf()] = coordinates;
		});

		var surroundings = board.getLiveNeighbours(field.coordinates);
		return !(surroundings.length < 2 || surroundings.length > 3);
	});

	// process dead neighbours
	for (var i in deadNeighbours) {
		if (deadNeighbours.hasOwnProperty(i)) {
			var coordinates = deadNeighbours[i];
			if (board.getLiveNeighbours(coordinates).length === 3) {
				fields.push(new Field(coordinates));
			}
		}
	}

	this.fields = fields;
};

/**
 * Fetch all live neighbour fields of given coordinates
 * @param   {Coordinates} coordinates
 * @returns {Array}
 */
Board.prototype.getLiveNeighbours = function(coordinates) {
	var possibilities = coordinates.getNeighbours();
	var board = this;
	return possibilities.map(function(coordinates) {
		return board.getField(coordinates);
	}).filter(function(field) {
		return field !== undefined;
	});
};

/**
 * Get all dead neighbour coordinates of given coordinates
 * @param   {Coordinates} coordinates
 * @returns {Array}
 */
Board.prototype.getDeadNeighbours = function(coordinates) {
	var possibilities = coordinates.getNeighbours();
	var board = this;
	return possibilities.filter(function(coordinates) {
		return board.getField(coordinates) === undefined;
	});
};
