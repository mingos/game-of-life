/**
 * N-dimensional coordinates object. Accepts any number of numeric arguments. Each consecutive argument will be the
 * coordinate in the next dimension, e.g. providing two arguments yields a two-dimensional Coordinates object.
 * @constructor
 */
var Coordinates = function() {
	this.axes = [];

	// assign coordinate values to all axes
	var i = 0, j = arguments.length;
	for(; i < j; ++i) {
		this.axes.push(arguments[i]);
	}
};

/**
 * Clone a Coordinates instance
 * @returns {Coordinates}
 */
Coordinates.prototype.clone = function() {
	var result = new Coordinates();
	result.axes = this.axes.slice(0);
	return result;
};

/**
 * Returns a primitive value of the Coordinates instance
 * @returns {String}
 */
Coordinates.prototype.valueOf = function() {
	return "Coordinates {axes: [" + this.axes.join(", ") + "]}";
};

/**
 * Check for coordinates equality. Coordinates are equal if they have the same number of dimensions and all axis
 * coordinates are identical.
 * @param   {Coordinates} coordinates
 * @returns {Boolean}
 */
Coordinates.prototype.equals = function(coordinates) {
	return coordinates.valueOf() === this.valueOf();
};

/**
 * Get all neighbouring coordinates
 * @returns {Array}
 */
Coordinates.prototype.getNeighbours = function() {
	var dimensions = this.axes.length;
	var result = [new Coordinates()];
	var me = this;

	for (var d = 0; d < dimensions; ++d) {
		(function(d, replacement) {
			result.forEach(function(coordinates) {
				var newResult = [
					coordinates.clone(),
					coordinates.clone(),
					coordinates.clone()
				];

				newResult.forEach(function(coordinates, idx) {
					coordinates.axes.push(me.axes[d] + idx - 1);
				});

				replacement = replacement.concat(newResult);
			});

			result = replacement;
		})(d, []);
	}

	return result.filter(function(coordinates) {
		return !me.equals(coordinates);
	});
};
