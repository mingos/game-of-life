/**
 * A live field
 * @param {Coordinates} coordinates
 * @constructor
 */
var Field = function(coordinates) {
	this.coordinates = coordinates;
};
